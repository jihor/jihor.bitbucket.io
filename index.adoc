= Размышления о ремонте
:toc: macro
:toc: left
:toclevels: 5
:toc-title: Оглавление

Наталья, Инна, добрый день! +
Здесь я собрал те фотографии, которые мне приглянулись в плане интерьера квартиры. +
 +

toc::[]

include::1.adoc[]

include::2.adoc[]

include::3.adoc[]

include::4.adoc[]