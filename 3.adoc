## 3. Подходящие картины
:imagesdir: ./images/3

### В гостиную
Графику у Дейнеки я не смотрел, только живопись. +
Дейнека - "Бригада на отдыхе": +
image:deineka_brigada_no_otdykhe_1952.jpg[width=600] +
 +
Дейнека - "Эстафета": +
image:deineka_estafeta_1947.png[width=600] +
 +
Дейнека - "Москва" - ну это просто так, для красоты, не буду же я себе то же самое, что маме вешать: +
image:deineka_moskva_1952.jpg[width=600] +
 +
Дейнека - "Раздолье" - картина написана в 1944 году, что возможно объясняет чисто женский состав на ней: +
image:deineka_razdolie_1944.png[width=600] +
 +
Нисский - "Подмосковный пейзаж": +
image:nissky_podmoskovny_peizazh.jpg[width=600] +
 +
Яковлев - "Советский транспорт": +
image:yakovlev_sovetsky_transport_1950.jpg[width=600] +
 +
Дейнека - "Хорошее утро": +
image:deineka_khoroshee_utro_1960.jpg[width=600] +
 +

### В спальню
Дейнека - "Крымский пейзаж": +
image:deineka_krymsky_peizazh_1930s.jpg[height=600] +
 +
Дейнека - "В воздухе": +
image:deineka_v_vozdukhe_1932.jpg[width=600] +
 +
Дейнека - "Юность": +
image:deineka_yunost_1961.png[width=600] +
 +
Дейнека - "Будущие лётчики" - ну это классика: +
image:deineka_budushie_letchiki_1938.jpg[width=600] +
 +
Дейнека - "Дорога на юг": +
image:deineka_doroga_na_yug_1930.jpg[width=600] +
 +
Дейнека - "Колхозница на велосипеде": +
image:deineka_kolkhoznitsa_na_velosipede_1935.jpg[width=600] +
 +


### Прочие картины, без привязки к месту в доме
Дейнека - "Дорога в Маунт-Вернон": +
image:dieneka_doroga_v_maunt_vernon_1935.jpg[width=600] +
 +
Нисский - "Над снегами" - темновата, но что-то в ней есть: +
image:nissky_nad_snegami_1960.jpg[width=600] +
 +
Нисский - "Осень. Семафоры" - неплохая картина: +
image:nissky_osen_semafory_1932.jpg[width=600] +
 +
Нисский - "Пейзаж" - душновата: +
image:nissky_peizazh.png[width=600] +
 +
Нисский - "В пути" - грустновата: +
image:nissky_v_puti_1964.png[width=600] +
 +

